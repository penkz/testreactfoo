# README

## Instalação 
O projeto está utilizando o Ruby versão 2.4.0, Rails versão 5.0.2, React 15.5.4
e Webpack 2.4.1

Estamos usando o RVM para rodar no ambiente de desenvolvimento.

Para rodar o projeto em development, após o clone do projeto em um ambiente
rodar os seguintes comandos:

Para instalar as dependências:
> gem install bundler

> gem install foreman

> bundle install && npm install

Para criar o banco de dados:
> rails db:create

> rails db:migrate

Para popular o banco com alguns dados:
> rails db:seed

Para rodar os testes (Rspec)
> rspec


Iniciando a aplicação em Development. Estamos usando o foreman para rodar o processo do rails e webpack ao mesmo tempo

> foreman start -f Procfile.dev

## Admin
Está instalado o RailsAdmin básico para criar novos posts

> http://localhost:3000/admin 
