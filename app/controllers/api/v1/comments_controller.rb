class Api::V1::CommentsController < Api::V1::BaseController 
  def index
    post = Post.find(params[:post_id])
    render json: post.comments.order('created_at DESC')
  end

  def create
    post = Post.find(params[:post_id])
    comment = post.comments.new(comment_params)
    if comment.save!
      render json: comment 
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:name, :body)
  end
end
