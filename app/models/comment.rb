class Comment < ApplicationRecord
  validates :name, presence: true
  validates :body, presence: true
  validates :post_id, presence: true

  belongs_to :post
end
