class PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :body, :author, :published, :created_at
end
