import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import App from './react/components/App'


document.addEventListener('DOMContentLoaded', () => {
  console.log('App Initialized')
  ReactDOM.render(
    <Router>
      <App />
    </Router>
    , document.querySelector("#app"));
})
