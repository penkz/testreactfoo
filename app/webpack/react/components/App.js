import React from 'react'
import { Route, Link } from 'react-router-dom'
import PostDetail from './PostDetail.jsx' 
import Home from './Home.jsx'

export default class App extends React.Component {
  constructor(props) {
    super(props)
      this.state = {
        posts : null 
      }
  }

  componentDidMount() {
    fetch('/api/v1/posts')
      .then(response => response.json())
      .then(posts => {
        this.setState({ posts: posts.data })
      })
  }

  render() {
    const { name } = "Rodrigo"
    const posts = this.state.posts 
    return(
      <div>
        <Link to="/">Home</Link>
        <Route exact path="/" render={() => (
        <Home posts={posts}/> 
        )} />

        <Route path="/posts/:post_id" render={({ match }) => (
          <PostDetail id={match.params.post_id}/>
        )}/>
      </div>
    )
  }
}
