import React from 'react'
import Comment from './Comment.jsx' 

export default class CommentBox extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      body: "",
      errors: null,
      comments: []
    }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
 
  componentDidMount() {
    fetch(`/api/v1/posts/${this.props.post_id}/comments`)
      .then(response => response.json())
      .then(comments => {
        this.setState({ comments: comments.data })
      })
  }

  handleInputChange(event) {
    const target = event.target
    const name = target.name
    const value = target.value

    this.setState({
      [name] : value
    })
  }

  handleSubmit (event) {
    var name = this.state.name;
    var body = this.state.body
    var errors = false
    var error_messages = []

    if (name.match(/\w+/) == null) {
      error_messages.push("O nome não pode ficar em branco")
      errors = true
    }

    if(body.match(/\w+/) == null){
      error_messages.push("A mensagem não pode ficar em branco")
      errors = true
    } 

    this.setState({ errors: error_messages })

    if(!errors) {
      var request = new Request(`/api/v1/posts/${this.props.post_id}/comments`, 
        { method: 'POST', headers: { "Content-Type" : "application/json" },
          body: `{ "comment" : {"name" : "${name}", "body" : "${body}" }}` })
     
    fetch(request)
      .then(response => response.json())
      .then(comment => {
        this.setState({ comments: [comment.data].concat(this.state.comments) })
      })

      this.setState({ errors: null, name: "", body: "" })
    }
    
    event.preventDefault();
  }

  render() {
    return(
      <div className="comment-box">
        <h3>Comentários</h3>
        {this.state.errors
        ?  <div className="errors"><p className="bg-danger">{this.state.errors}</p></div>
          : ""
        }
        <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label for="name">Nome:</label>
          <input name="name" id="name" type="text" className="form-control" value={this.state.name} onChange={this.handleInputChange} />
        </div>
        <div className="form-group">
          <label for="body">Comentário:</label>
          <textarea name="body" id="body" className="form-control" onChange={this.handleInputChange} value={this.state.body}></textarea>
        </div>
        <p>
        <button className="btn btn-primary">Enviar</button>
        </p>
        {this.state.comments.length < 1
         ? "Não há nenhum comentário"
         : this.state.comments.map((comment) =>
          <Comment name={comment.attributes.name} body={comment.attributes.body} key={comment.id} />
         ) 
        }
        </form>
      </div>
    )
  }
}
