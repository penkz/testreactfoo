import React from 'react'
import Post from './Post.jsx'

const Home = (props) => {
    const posts = props.posts
    return (
      <div>
        <div className="posts">
        {!posts
          ? "Loading..."
          : posts.map((post) =>
              <Post key={post.id} id={post.id} title={post.attributes.title} body={post.attributes.body} linkTitle={true}/>
            )
        }
        </div>
      </div>
    )
}

export default Home
