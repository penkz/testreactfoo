import React from 'react'
import { Link } from 'react-router-dom'

export default class Post extends React.Component {
  render() {
    return (
      <div className="post">
          { this.props.linkTitle
            ? <h2><Link to={`/posts/${this.props.id}`}>{this.props.title}</Link></h2>
            : <h2>{this.props.title}</h2>
          }
          <p>{this.props.body}</p>
        </div>
      )
  }
}
