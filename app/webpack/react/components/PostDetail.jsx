import React from 'react'
import { Link } from 'react-router-dom'
import CommentBox from './CommentBox.jsx' 
import Post from './Post.jsx'

export default class PostDetail extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      post: null
    }
  }

  componentDidMount() {
    fetch(`/api/v1/posts/${this.props.id}`)
      .then(response => response.json())
      .then(post => {
        this.setState({ post: post.data })
      })
  }

  render() {
    const post = this.state.post
    return (
      <div>
      {!post
        ? "Loading"
        : <Post id={this.props.id} title={post.attributes.title} body={post.attributes.body} />
      }
      <CommentBox post_id={this.props.id} />
      <Link to="/">Voltar</Link>
      </div>
    )
  }
}
