Rails.application.routes.draw do
  root "home#index"
  match '*path', to: 'home#index', via: :all, constraints: { path: /(?!.*(api|admin)).*/ } 

  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resources :posts, only: [:index, :show] do
        resources :comments, only: [:index, :create]
      end
    end
  end

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
