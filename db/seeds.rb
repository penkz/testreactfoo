# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


posts = [
  {
    title: 'This is a great post',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu orci in augue ullamcorper viverra ut id nisi. Morbi vestibulum volutpat nisi, nec mollis sem. Sed erat orci, pharetra a accumsan eget, tempor eu sem.',
    author: 'John Coltrane',
    published: true
  },
  {
    title: 'This is another great post',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu orci in augue ullamcorper viverra ut id nisi. Morbi vestibulum volutpat nisi, nec mollis sem. Sed erat orci, pharetra a accumsan eget, tempor eu sem.',
    author: 'Miles Davis',
    published: true
  },
  {
    title: 'This one not so much...',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu orci in augue ullamcorper viverra ut id nisi. Morbi vestibulum volutpat nisi, nec mollis sem. Sed erat orci, pharetra a accumsan eget, tempor eu sem.',
    author: 'Charles Mingus',
    published: true
  }
]

comments = [
  {
    name: "Charlie Parker",
    body: "Hello, this is my comment"
  },
  {
    name: "Duke Ellington",
    body: "It don't mean a thing, if ir ain't got that swing"
  },
  {
    name: "Ella Fitzgerald",
    body: "Whatever, whatever... whatever."
  }
]

posts.each do |post|
  p = Post.create(post)
  p.comments.create(comments)
end

