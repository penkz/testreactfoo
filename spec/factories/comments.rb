FactoryGirl.define do
  factory :comment do
    sequence(:name) { |n| "User #{n}" }
    body "MyText"
    association :post, factory: :post
  end
end
