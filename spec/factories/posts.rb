FactoryGirl.define do
  factory :post do
    sequence(:title) { |n| "My new blog post #{n}" }
    body "This new post is awesome!" 
    published true
    author "Rodrigo Penloski"
  end
end
