require 'rails_helper'

RSpec.describe Comment, type: :model do

  context "validations" do
    it { should validate_presence_of :name }
    it { should validate_presence_of :body }
    it { should validate_presence_of :post_id }
  end

  context "associations" do
    it { should belong_to :post }
  end
end
