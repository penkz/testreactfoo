require 'rails_helper'

RSpec.describe Api::V1::CommentsController, type: :request do
  describe "#create" do
    let(:post_obj) { FactoryGirl.create(:post, published: true) }
    context "with valid data" do
      before(:each) do
        post api_v1_post_comments_path(post_obj), params: { comment: { name: "Rodrigo", body: "Hello" } }
      end

      it "creates a new comment" do
        expect(post_obj.comments.count).to eq 1 
      end 
    end

    context "with invalid data" do
      before(:each) do
        post api_v1_post_comments_path(post_obj), params: { comment: { name: "", body: "" } }
      end

      it "returns unprocessable entity" do
        expect(response).to have_http_status :unprocessable_entity
      end

      it "does not create a comment" do
        expect(post_obj.comments.count).to eq 0 
      end

      it "returns the error messages" do
        expect(json).to have_key "errors" 
      end
    end
  end
end
