require 'rails_helper'

RSpec.describe Api::V1::CommentsController, type: :request do
  describe "#index" do
    let!(:post_obj) { FactoryGirl.create(:post, published: true) }
    
    context "when comments exist" do
      before { post_obj.comments.create name: "Rodrigo", body: "Body" } 
      before(:each) do
        get api_v1_post_comments_path(post_obj)
      end

      it { expect(response).to have_http_status 200 }

      it "lists only published posts" do
        expect(json["data"].length).to eq 1 
      end 

      it "data is of comments type" do
        expect(json["data"][0]["type"]).to eq "comments"
      end 

      it "has a name attribute" do
        expect(json["data"][0]["attributes"]).to have_key "name"
      end 

      it "has a body attribute" do
        expect(json["data"][0]["attributes"]).to have_key "body"
      end 
    end
  end
end
