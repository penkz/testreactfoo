require 'rails_helper'

RSpec.describe Api::V1::PostsController, type: :request do
  describe "#index" do
    let!(:post) { FactoryGirl.create(:post, published: true) }
    let!(:unpublished_post) { FactoryGirl.create(:post, published: false) }

    context "when posts exist" do
      before(:each) do
        get api_v1_posts_path
      end

      it { expect(response).to have_http_status 200 }

      it "lists only published posts" do
        expect(json["data"].length).to eq 1 
      end 

      it "data is of posts type" do
        expect(json["data"][0]["type"]).to eq "posts"
      end 

      it "has a title attribute" do
        expect(json["data"][0]["attributes"]).to have_key "title"
      end

      it "has a body attribute" do
        expect(json["data"][0]["attributes"]).to have_key "body"
      end

      it "has an author attribute" do
        expect(json["data"][0]["attributes"]).to have_key "author"
      end

      it "has a created-at attribute" do
        expect(json["data"][0]["attributes"]).to have_key "created-at"
      end

      it "has a published attribute" do
        expect(json["data"][0]["attributes"]).to have_key "published"
      end
    end
  end
end
