require 'rails_helper'

RSpec.describe Api::V1::PostsController, type: :request do
  describe "#show" do
    let!(:post) { FactoryGirl.create(:post, published: true) }

    context "when posts exist" do
      before(:each) do
        get api_v1_post_path(post.id)
      end

      it { expect(response).to have_http_status 200 }

      it "data is of posts type" do
        expect(json["data"]["type"]).to eq "posts"
      end 

      it "has a title attribute" do
        expect(json["data"]["attributes"]).to have_key "title"
      end

      it "has a body attribute" do
        expect(json["data"]["attributes"]).to have_key "body"
      end

      it "has an author attribute" do
        expect(json["data"]["attributes"]).to have_key "author"
      end

      it "has a created-at attribute" do
        expect(json["data"]["attributes"]).to have_key "created-at"
      end

      it "has a published attribute" do
        expect(json["data"]["attributes"]).to have_key "published"
      end
    end

    context "when post does not exist" do
      before(:each) do
        get api_v1_post_path(23)
      end

      it { expect(response).to have_http_status 404 }

      it "has an error attribute" do
        expect(json).to have_key "error"
      end
    end
  end
end
