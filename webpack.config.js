const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: "./app/webpack/entry.js",
  output: {
    path: path.join(__dirname,"app/assets/javascripts/"),
    filename: "webpack-bundle.js"
  },
  module: {
    loaders: [
      { test: /\.js(x)?$/,
        loader: "babel-loader",
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin()
  ]
};
